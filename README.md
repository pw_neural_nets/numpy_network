# Multilayer Perceptron

The neural network was implemented in numpy without any neural network frameworks.
Architecture is fully customizable. Parameters like number of layers, 
the number of neurons per layer etc. can be specified in the config or experiment script.
This implementation was evaluated on MNIST dataset and achieved 97.87% accuracy
on the test set.