import random
import numpy as np
import pandas as pd
from pathlib import Path
import seaborn as sns
import matplotlib.pyplot as plt

from data_processing import model_input
from network import np_automatic_dense
from network import metrics
from network import optimizers
from pipelines import single_experiment

SEED = 2137
random.seed(SEED)
np.random.seed(SEED)

# paths
ROOT_DIR = Path("projekt1-oddanie")
CLS_DIR = ROOT_DIR/"clasification"
CLS_FILE = CLS_DIR/"data.circles.train.1000.csv"

mode = 'cls'

df_cls = pd.read_csv(CLS_FILE)
train_data = df_cls.values

# net hpars
layer_shapes = [2, 20, 20, 4]
init = 'he'
activation_func = 'relu'
loss_func = 'MSE'
metric = metrics.accuracy

# train hpars
epochs = 10
batch_size = 10
lr = 1.
test_frac = 0.02

model = np_automatic_dense.DenseNetwork(
    layer_shapes=layer_shapes,
    loss_func=loss_func,
    activation_func=activation_func,
    init=init,
    mode=mode
)

preprocessor = model_input.preprocessors[mode](test_frac, verbose=True)
optimizer = optimizers.OptimizerSGD()

experiment_pipe = single_experiment.ExperimentPipeline(model, preprocessor, optimizer, metric, verbose=True)

experiment_pipe.train(train_data, epochs, batch_size, lr)


df_cls['cls2'] = df_cls['cls'] - 1
sns.scatterplot(x='x', y='y', hue='cls2', data=df_cls)
plt.show()


X = df_cls[['x', 'y']].values
Y_hat = experiment_pipe.predict(X)

sns.scatterplot(x='x', y='y', hue=Y_hat, style='cls2', data=df_cls)
plt.show()


CLS_TEST_FILE = CLS_DIR/"data.circles.test.1000.csv"
test_df_cls = pd.read_csv(CLS_TEST_FILE)
X = test_df_cls[['x', 'y']].values
Y_hat = experiment_pipe.predict(X)
test_df_cls['cls2'] = test_df_cls['cls'] - 1
sns.scatterplot(x='x', y='y', hue=Y_hat, style='cls2', data=test_df_cls)
plt.show()