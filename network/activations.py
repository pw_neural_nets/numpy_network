import numpy as np


def sigmoid(z):
    return 1. / (1. + np.exp(-z))


def sigmoid_prime(z):
    s = sigmoid(z)
    return s * (1. - s)


def relu(z):
    return np.where(z > 0, z, 0)


def relu_prime(z):
    return np.where(z > 0, 1, 0)


def tanh(z):
    return np.tanh(z)


def tanh_prime(z):
    return 1 - tanh(z) ** 2


act_funcs = {
    'sigmoid': sigmoid,
    'relu': relu,
    'tanh': tanh,
}

dir_act_funcs = {
    'sigmoid': sigmoid_prime,
    'relu': relu_prime,
    'tanh': tanh_prime,
}
