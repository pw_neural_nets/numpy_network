import random
import numpy as np

from network.activations import act_funcs
from network.activations import dir_act_funcs
from network.loss_functions import loss_funcs
from network.initializations import init_funcs
from network.graphics import draw_graph


class DenseNetwork:
    '''Arch, backward and forward moves'''
    # softmax zamiast sigmoida do klasyfikacji
    # bo jest w stanie przewidziec [1, 1] gdzie [1, 0] oznacza klase 0 a [0, 1] klase 1
    supported_modes = ['reg', 'cls']

    def __init__(self, layer_shapes, loss_func, l1=0, l2=0, activation_func='sigmoid', init='random', mode='cls'):
        self.num_layers = len(layer_shapes)
        self.layer_shapes = layer_shapes
        self.biases = [init_funcs[init](y, 1) for y in layer_shapes[1:]]
        self.weights = [init_funcs[init](y, x)
                        for x, y in zip(layer_shapes[:-1], layer_shapes[1:])]
        self.activation_func = activation_func
        self.loss_func = loss_funcs[loss_func]

        self.l1 = l1
        self.l2 = l2

        if mode in self.supported_modes:
            self.mode = mode
        else:
            raise ValueError(f'Mode {mode} is not supported. Supported modes: {self.supported_modes}')

    def feedforward(self, a):
        for b, w in list(zip(self.biases, self.weights))[:-1]:
            a = act_funcs[self.activation_func](np.dot(w, a) + b)

        b, w = self.biases[-1], self.weights[-1]
        a = np.dot(w, a) + b
        a = a if self.mode == 'reg' else act_funcs['sigmoid'](a)

        return a

    def backprop(self, x, y):
        layer_errors = []
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        activation = x
        activations = [x]
        zs = []

        for b, w in list(zip(self.biases, self.weights))[:-1]:
            z = np.dot(w, activation) + b
            zs.append(z)
            activation = act_funcs[self.activation_func](z)
            activations.append(activation)

        b, w = self.biases[-1], self.weights[-1]
        z = np.dot(w, activation) + b
        zs.append(z)
        activation = z if self.mode == 'reg' else act_funcs['sigmoid'](z)
        activations.append(activation)

        backward_act_dir = 1 if self.mode == 'reg' else dir_act_funcs['sigmoid'](zs[-1])

        dir_loss = self.loss_func.prime(activations[-1], y)
        loss = self.loss_func.get_loss(activations[-1], y) + self.get_l12_all_layers()

        delta = dir_loss * backward_act_dir
        nabla_b[-1] = delta

        layer_error = np.sum(self.weights[-1].T @ delta)
        layer_errors.append(layer_error)

        nabla_w[-1] = np.dot(delta, activations[-2].T) + self.get_l1(self.weights[-1]) + self.get_l2(self.weights[-1])

        for l in range(2, self.num_layers):
            z = zs[-l]
            sp = dir_act_funcs[self.activation_func](z)
            delta = np.dot(self.weights[-l + 1].T, delta) * sp

            layer_error = np.sum(self.weights[-l].T @ delta)
            layer_errors.append(layer_error)

            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, activations[-l - 1].T) + self.get_l1(self.weights[-l]) + self.get_l2(self.weights[-l])

        errors = {'loss': loss, 'layer_errors': layer_errors}

        return (nabla_b, nabla_w), errors

    def predict_logit(self, x):
        return self.feedforward(x)

    def predict_label(self, x):
        logit = self.predict_logit(x)
        return np.argmax(logit, axis=1)

    def predict(self, X):
        if self.mode == 'cls':
            y_hat = np.argmax(np.squeeze([self.predict_logit(x) for x in X]), axis=1)
        else:
            y_hat = np.array([self.predict_logit(x[0]) for x in X])
        return y_hat
    
    def draw(self, file):
        neurons = {}
        curr = 0
        for i, layer_size in enumerate(self.layer_shapes):
            for j in range(layer_size):
                neurons[(i, j)] = curr
                curr += 1
        biases = []
        edges = []
        for i, layer_i in enumerate(self.weights):
            for j, out_j in enumerate(layer_i):
                for k, weight in enumerate(out_j):
                    edges.append((neurons[(i, k)], neurons[(i + 1, j)], weight))
        for (layer_num, neuron_in_layer), neuron_num in neurons.items():
            if layer_num == 0:
                continue
            biases.append((neuron_num, self.biases[layer_num - 1][neuron_in_layer][0]))
        draw_graph(self.layer_shapes, edges, biases, file)

    def get_l12_all_layers(self):
        w_l12 = 0
        for w in self.weights:
            w_l1 = self.get_l1(w, derivative=False)
            w_l2 = self.get_l2(w, derivative=False)
            w_l12 += np.sum(w_l1 + w_l2)
        return w_l12

    def get_l1(self, w, derivative=True):
        if not self.l1:
            return 0

        if derivative:
            mask_pos = (w >= 0) * 1
            mask_neg = (w < 0) * -1
            w = mask_neg + mask_pos
            return np.mean(w) * self.l1
        else:
            return np.mean(np.abs(w)) * self.l1

    def get_l2(self, w, derivative=True):
        if not self.l2:
            return 0

        if derivative:
            return np.mean(w) * self.l2
        else:
            return np.mean(w**2) * self.l2
