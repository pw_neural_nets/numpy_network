import numpy as np


def accuracy(Y_hat, Y):
    assert Y.shape == Y_hat.shape
    Y_hat = np.argmax(Y_hat, axis=1)
    Y = np.argmax(Y, axis=1)
    return np.sum(np.equal(Y_hat, Y))/len(Y_hat) # is there also a potential problem with dimentions


def distance(Y_hat, Y):
    assert Y.shape == Y_hat.shape
    return np.mean((Y_hat - Y)**2)
