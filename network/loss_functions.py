import numpy as np


class MSE:
    def get_loss(self, Y_hat, Y):
        assert Y.shape == Y_hat.shape
        return np.mean((Y_hat - Y)**2)

    def prime(self, Y_hat, Y):
        assert Y.shape == Y_hat.shape
        return 2 * (Y_hat - Y)


class MAE:
    def get_loss(self, Y, Y_hat):
        assert Y.shape == Y_hat.shape
        return np.mean(np.abs(Y_hat - Y))

    def prime(self, Y_hat, Y):
        assert Y.shape == Y_hat.shape
        return np.where(Y_hat - Y > 0, 1, -1)


## classification
# def binary_cross_entropy(y, y_hat):
#     entropy = y*log(y_hat) + (1-y)*log(1-y_hat)
#     return -1 * np.mean(entropy)

## https://stats.stackexchange.com/questions/370723/how-to-calculate-the-derivative-of-crossentropy-error-function
# def CrossEntropy(yHat, y):
#     if y == 1:
#       return -log(yHat)
#     else:
#       return -log(1 - yHat)

# def multilabel_cross_entropy():
#     pass

loss_funcs = {
    'MSE': MSE(),
    'MAE': MAE(),
}