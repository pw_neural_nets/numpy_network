import numpy as np


# weights -> 0 -> slow gradient updates of sigmoid
def zero_weight_init(in_features, out_features):
    return np.zeros(in_features, out_features)


# weights -> oo -> slow gradient updates of sigmoid
def large_weight_init(in_features, out_features):
    return np.random.randn(in_features, out_features) * 10e3


# weights -> random -> kinda works but can do better
def random_weight_init(in_features, out_features):
    return np.random.randn(in_features, out_features)


# Xavier tanh http://proceedings.mlr.press/v9/glorot10a/glorot10a.pdf
def xavier_weight_init(in_features, out_features):
    return np.random.randn(in_features, out_features) / np.sqrt(in_features)


# He for RELU https://arxiv.org/pdf/1502.01852.pdf
def he_weight_init(in_features, out_features):
    sigma = np.sqrt(2 / in_features)
    return np.random.randn(in_features, out_features) * sigma


init_funcs = {
    'zero': zero_weight_init,
    'random': random_weight_init,
    'xavier': xavier_weight_init,
    'he': he_weight_init,
}