import numpy as np

class LearningScheduler:
    def __init__(self, start_lr, end_lr, sample_count, epochs, test_frac, batch_size, strat='lin', warmup_frac=0.0):
        self.start_lr = start_lr
        self.end_lr = end_lr
        self.max_step = int(sample_count * epochs * (1-test_frac) / batch_size)
        self.warmup_size = int(self.max_step * warmup_frac)

        self.max_decay_step = int(self.max_step - self.warmup_size)
        self.strat = self.pick_stategy(strat)

    def __iter__(self):
        for step in range(0, self.max_step):
            if step < self.warmup_size:
                lr = self.get_warmup(step)
            else:
                lr = self.strat(step)
            yield lr

    def pick_stategy(self, strat):
        if strat == 'lin':
            return self.linear_decay
        elif strat == 'cos':
            return self.cos_decay
        elif strat == 'const':
            return lambda x: self.start_lr

    def get_warmup(self, step):
        return step / self.warmup_size * self.start_lr

    def linear_decay(self, step):
        step -= self.warmup_size
        return (self.start_lr - self.end_lr) * (self.max_decay_step - step) / self.max_decay_step + self.end_lr

    def cos_decay(self, step):
        step -= self.warmup_size
        return (self.start_lr - self.end_lr) * (np.cos(np.pi * (1 - (self.max_decay_step - step) / self.max_decay_step))+1)/2 + self.end_lr
