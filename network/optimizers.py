import numpy as np

class OptimizerSGD:
    '''return changes that should be made in the model paramters'''

    def optimize(self, net, data, lr):
        nabla_b = [np.zeros(b.shape) for b in net.biases]
        nabla_w = [np.zeros(w.shape) for w in net.weights]

        batch_layers_errors = []
        batch_loss = []
        for x, y in data:
            # shitty coupled
            (delta_nabla_b, delta_nabla_w), errors = net.backprop(x, y)
            nabla_b = [nb + dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
            nabla_w = [nw + dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]
            batch_layers_errors.append(errors['layer_errors'])
            batch_loss.append(errors['loss'])

        mean_batch_layers_error = np.mean(abs(np.array(batch_layers_errors)), axis=0)
        batch_loss = np.mean(batch_loss)

        net.weights = [w - (lr / len(data)) * nw
                       for w, nw in zip(net.weights, nabla_w)]
        net.biases = [b - (lr / len(data)) * nb
                      for b, nb in zip(net.biases, nabla_b)]

        return mean_batch_layers_error, batch_loss
