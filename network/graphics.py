import networkx as nx
import matplotlib.pyplot as plt

# nodes - array of numbers of neurons in layer
# edges - array of tuples (from_node, to_node, weight)
# biases - array of tuples (to_node, weight)
def draw_graph(nodes, edges, biases, file):
    G = nx.DiGraph()
    node_counter = 0
    graph_width = 2*max(nodes)
    #add nodes
    for i in range(0, len(nodes)):
        nodes_in_layer = nodes[i]
        margin_quant = graph_width / (float(nodes_in_layer) + 1)
        for j in range(0, nodes_in_layer):
            G.add_node(node_counter, pos=(margin_quant * (j + 1), i))
            node_counter = node_counter + 1
    #add edges
    for edge in edges:
        G.add_edge(edge[0], edge[1], weight=round(edge[2], 4))
    pos=nx.get_node_attributes(G,'pos')
    #add biases
    for bias in biases:
        node_pos = pos[bias[0]]
        G.add_node(node_counter, pos=(node_pos[0], node_pos[1] - 0.5))
        G.add_edge(node_counter, bias[0], weight=round(bias[1], 4))
        node_counter = node_counter + 1
    f = plt.figure(figsize=(14,14)) 
    pos=nx.get_node_attributes(G,'pos')
    nx.draw(G, pos)
    labels = nx.get_edge_attributes(G,'weight')
    nx.draw_networkx_edge_labels(G,pos,labels, label_pos=0.7)
    f.savefig(file)
    plt.close(f)