from pathlib import Path
import pandas as pd
import numpy as np
import pickle
import matplotlib.pyplot as plt

# train_data_path = Path('./mnist/mnist_train.csv')
# test_data_path = Path('./mnist/mnist_test.csv')
#
# df_train = pd.read_csv(train_data_path)
# df_test = pd.read_csv(test_data_path)
#
# def save_df(df, path):
#     Y = df['label'].values
#     X = df.iloc[:, 1:].values
#     X = (X - np.min(X)) / (np.max(X) - np.min(X))
#
#     import pdb;pdb.set_trace()
#
#     with open(path, 'wb') as f:
#         pickle.dump((X, Y), f)
#
# save_df(df_train, 'normalized_train_mnist.pkl')
# save_df(df_test, 'normalized_test_mnist.pkl')


import gzip
train_data_file = 'mnist/train-images-idx3-ubyte.gz'
train_label_file = 'mnist/train-labels-idx1-ubyte.gz'
train_target = 'mnist/train_mnist.pkl'

test_data_file = 'mnist/t10k-images-idx3-ubyte.gz'
test_label_file = 'mnist/t10k-labels-idx1-ubyte.gz'
test_target = 'mnist/test_mnist.pkl'



def convert_to_pkl(data_file, label_file, target_path):
    with gzip.open(data_file, 'rb') as f:
        X = np.frombuffer(f.read(), np.uint8, offset=16)

    X = X.reshape(-1, 784)

    scaled_X = (X - np.min(X)) / (np.max(X) - np.min(X))

    with gzip.open(label_file, 'rb') as f:
        Y = np.frombuffer(f.read(), np.uint8, offset=8)

    with open(target_path, 'wb') as f:
        pickle.dump((scaled_X, Y), f)

convert_to_pkl(train_data_file, train_label_file, train_target)
convert_to_pkl(test_data_file, test_label_file, test_target)



