# Rozwiązywanie zadania klasyfikacji i regresji z wykorzystaniem perceptronu wielowarstwowego 

Autorzy: Jakub Czyżniejewski, Renard Korzeniowski   

Celem zadania projektowego była implementacja perceptronu wielowarstwowego i rozwiązanie za jego pomocą zagadnień klasyfikacji i regresji dla zbiorów danych dostarczonych przez prowadzącego zajęcia. Częścią zadania była także implementacja uczenia sieci algorytmem propagacji wstecznej błędu. Zadanie wykonano z wykorzystaniem języka Python 3. 

## Perceptron wielowarstwowy

Perceptron wielowarstwowy jest najpopularniejszym typem sztucznych sieci neuronowych i stanowi naturalne rozwinięcie koncepcji perceptronu. Sam perceptron składa się z jednego lub wielu niezależnych neuronów McCullocha-Pittsa. Perceptron taki jest w stanie poprawnie klasyfikować dane ze zbiorów liniowo separowalnych. Wskazanie tego ograniczenia spowodowało w latach 70. utratę zainteresowania sztucznymi sieciami neuronowymi w środowisku naukowym. Perceptron wielowarstwowy jest pozbawiony tego ograniczenia. Jest to sieć składająca się z neuronów zorganizowanych w warstwy w taki sposób, że wyjścia neuronów z danej warstwy są połączone z wejściami neuronów z warstwy następnej. Perceptron wielowarstwowy składa się z warstwy wejściowej, wyjściowej i pewnej liczby tzw. warstw ukrytych. Każde połączenie między neuronami posiada przypisaną pewną wagę, która określa jaki dany neuron ma wpływ na stan następnego neuronu. W trakcie działania sieci sygnał przesyłany jest przez neurony z kolejnych warstw. W każdym neuronie następuje sumowanie sygnałów wejściowych z odpowiednimi wagami oraz aplikowana jest na otrzymaną sumę tzw. funkcja aktywacji, która zwraca wartość sygnału wyjściowego dla neuronu. Ta wartość staje się sygnałem wejściowym dla kolejnej warstwy i proces ten postępuje aż do warstwy wyjściowej. Uczenie sieci to proces dobierania odpowiednich dla zagadnienia wag połączeń. W tym projekcie uczenie sieci odbywa się z wykorzystaniem algorytmu propagacji wstecznej błędu. Jest to metoda polegająca na modyfikowaniu wag na połączeniach neuronów w kolejnych warstwach rozpoczynając od końca. Za błąd w warstwie wyjściowej uznany jest uchyb sieci, natomiast błąd wcześniejszych warstw jest iloczynem błędu z warstwy następnej i wag krawędzi. Takie podejście do liczenia błędu powoduje większą korektę dla neuronów mających większy wpływ (wyższą wagę) na zaburzenie wartości neuronu z warstwy następnej.

# Wyniki eksperymentów

Naszymi celami przy prowadzeniu eksperymentów było:

- znalezienie optymalnych parametrów uczenia dla zbiorów danych 

- zwizualizowanie procesu uczenia się sieci. 


## Podejscie do problemu 
Aby znaleźć optymalne parametry stworzyliśmy pipeline, który po otrzymaniu zbioru zestawów parametrów trenuje modele i zapisuje rezultaty dla każdego zestawu. Aby przeanalizować wyniki przeszukujemy przestrzeń eksperymentów ustalając część parametrów. Reszta parametrów jest uwolniona. Wyniki eksperymentów sortujemy po wartości metryki mierzacej błąd dla zbioru danych. Znalezione w ten sposób wartości parametrów służą nam jako wartości domyślne w dalszej części raportu, gdy porównujemy zachowanie sieci pod kątem jedynie części parametrów.

Przykładowy wypis najlepszych parametrów przy przeszukaniu rezulatów:

```
[1.0, 1.0, 1.0, 1.0, 1.0]
{'activation_func': 'relu', 'batch_size': 1, 'epochs': 5, 'init': 'he', 'loss_func': <network.loss_functions.MSE object at 0x7f46979d87f0>, 'lr': 10, 'layer_shapes': [2, 8, 8, 3]}
[1.0, 1.0, 1.0, 1.0, 1.0]
{'activation_func': 'relu', 'batch_size': 1, 'epochs': 5, 'init': 'random', 'loss_func': <network.loss_functions.MSE object at 0x7f4690558438>, 'lr': 10, 'layer_shapes': [2, 8, 8, 3]}
```

## Przeszukiwanie podprzestrzeni paramterów

### Objasnienie parametrów i wykresów w eksperymentach

- `lr` – krok uczenia, czyli mnożnik, który determinuje jak bardzo podatne na korektę są wagi połączeń
- `batch_size` – liczba punktów, z których zostaje wyliczony średni gradient użyty do zaktualizowania wag sieci 
- `init` – funkcja, która jest użyta do wybrania inicjalnych wag w sieci 
- `loss_func` – funkcja błędu - różniczkowalna ciągła funkcja, na podstawie której wyznaczamy dystans pomiędzy oczekiwaną wartością a predykcją sieci 
- `activation_func` – funkcja, która na podstawie wartości na wejściu neuronu oblicza wartość jego wyjścia.
- `layer_shapes` – liczba neuronów w każdej kolejnej warstwie (z uwzględnieniem warstw wejściowej i wyjściowej)

### Uzyte funkcje inicjalizacji wag

- `random` - wagi sieci przy tej inicjalizacji są wybierane losowo z rozkładu normalnego o średniej jeden i wariancji zero

- `xavier` - procedura inicjalizacji wag jest identyczna jak w przypadku `random` poza finalnym krokiem, w którym dzielimy wszystkie wartości przez pierwiastek z liczby wag wejścia. Ma to na celu sprawienie, aby wariancja warstw w sieci pozostała taka sama dla wszystkich warstw. Dzięki temu możliwe jest używanie głębszych sieci i uniknięcia problemu z wybuchającym i zanikającym gradientem. Teoria stojąca za tym została opsiana w [tej pracy](http://proceedings.mlr.press/v9/glorot10a/glorot10a.pdf) i zakładała użycia tanh jako funkcji aktywacji w seci

- `he` -  odpowiednik inicjalizacji analogiczna do `Xavier` jedynie stała, przez którą dzielimy jest dwa razy mniejsza. Jest ona stosowna gdy funkcja aktywacji sieci to relu, a nie tanh. Opisane jest to dokładniej w [tej pracy](https://arxiv.org/pdf/1502.01852.pdf)

### Opis środowiska eksperymentów

Metryka użyta do oceny modelu:

- klasyfikacja: dokładność 
- regresja: średni błąd kwadratowy

Dla każdego z eksplorowanych parametrów wygenerowane zostały po trzy pod wykresy

- `test_loss` – wartość funkcji straty dla próbek nie widzianych wcześniej przez model. Ta wartość jest liczona raz na epokę. Liczba epok przedstawiona jest na osi X. 
- `train_loss` – wartość funkcji straty dla próbek, na których model jest trenowany. Na osi X znajduje się numer danej grupy próbek i. e. `batch` dla, której została policzona uśredniona wartość zwrócona przez funkcję straty 
- `test_metric_result` – wartość metryki dla próbek nie widzianych wcześniej przez model. Analogicznie jak `test_loss` wartość ta jest liczona raz na epokę, a na osi X znajduję się numer epoki

Każdy z przedstawionych w eksperymentach wykresów opisuje zależność trzech śledzonych przez nas wartości i. e. `test_loss`, `train_loss` i `test_metric_result` od różnych wartości danego parametru. Różne wartości parametrów zostały zwizualizowane za pomocą kolorowych krzywych. Jednak, aby wytrenować sieć potrzebne jest więcej argumentów. Brane są one ze słownika `Domyślne paramtery`, który musi być podany dla każdego zbioru eksperymentów. `Domyślne paramtery` zostały wyznaczone na podstawie najlepszych wyników przy przeszukaniu dużej podprzestrzeni parametrów

### Parametry ekspetymentów dla klasyfikacji

Zbiór danych na którym przeprowadzilismy przeszukiwanie parametrów to

- `experiment_data.three_gauss.train.500.csv`

`Domyślne paramtery klasyfikacji`

- `activation_func`: 'relu',
- `batch_size`: 10,
- `init`: 'he',
- `loss_func`: ‘MSE’,
- `lr`: 0.1,
- `layer_shapes`: [2, 8, 8, 3]

### Wizualizacja i analiza wyników dla klasyfikacji

![png](output_3_0.png)

    Parametr `lr`=10 jest zbyt duży co możemy zaobserwować poprzez wartosc funkcji straty, która nie spada na zbiorze testowym oraz nie zbiega dla zbioru uczącego. Wartość `lr`=1 wydaję się być optymalną, strata dla niej na `trainset` i `testset` podczas przebiegu eksperymentu juz przy pierwszej epoce osiaga niska wartosc i taka pozostaje. Pozostałe kroki uczenia nie zdążyły zbiec do minimum w ciągu 5 epok.

![png](output_3_1.png)

    Na wykresie powyżej można zaobserwować, że `batch size`=1 zbiegł najszybciej do najmniejszej straty na `testset`. Wartość 100 zbiegała wolniej i uzyskała finalnie wyższą wartość straty. Analogiczną relację możemy zaobserwować pomiędzy 100 i 10 co między 1 i 100

![png](output_3_3.png)

    Widzimy, że przy zastosowaniu losowej inicjalizacji nasz trening nie zbiega do minimum natomiast po przeskalowaniu wariancji przy inicjalizacji do intputu otrzymaliśmy dobrę wyniki

![png](output_3_2.png)

	Na wykresie powyżej widać, że funkcja aktywacji `relu` zbiegła szybciej oraz uzyskała finalnie niższa wartość straty, niż sigmoid. Wynik ten jest jednak mocno zależny od innych parametrów takich jak np. inicjalizacja wag oraz data set co będzie można wywnioskować z wyników przedstawionych w dalszej części raportu

![png](output_5_0.png)

	Z wykresu powyżej możemy wywnioskować, że dla na tyle prostego problemu rozmiar sieci nie miał aż tak dużego znaczenia. Mimo to możemy zauważyć, że zbyt duża sieć nadmiernie dopasowała się do danych treningowych (efekt przeuczenia) i uzyskała gorsze wyniki dla `test_metric_results`

![png](output_7_0.png)

	Widzimy, że obie testowane funkcje błędu uzyskały porownywalne rezultaty. Możemy zauważyć, że dla MAE zbiegła odrobinę szybciej. W przypadku funkcji straty sieć miała inny rozmiar niż wcześniej porównywane parametry i.e. `layer_shapes` = [2, 4, 4, 3]


## Parametry ekspetymentów dla regresji

Zbiór danych na którym przeprowadzilismy przeszukiwanie parametrów

- `experiment_data.cube.train.500.csv`

W przypadku regresji w przeciwieństwie do klasyfikacji staramy się minimalizować zadaną metrykę przedstawioną na podwykresach podpisanych `test_metric_result` 

`Domyślne parametry`

- `activation_func`: 'relu',
- `batch_size`: 10,
- `init`: 'he',
- `loss_func`: ‘MSE’,
- `lr`: 0.1,
- `layer_shapes`: [1, 2, 1]

## Wizualizacja i analiza wyników dla regresji

![png](reg_lr.png)

	Z wykresu została usunięta krzywa dla `lr`=10 ze względu na wybuchający loss. Z pozostałych wartości widzimy, że 0.1 jest zdecydowanie najlepszą wartoscią dla kroku uczenia

![png](reg_bs.png)

	Ciekawym wynikiem jest to, że `batch size`=10 uzyskał dwukrotnie niższą wartość `test_metric_results` niż pozostałe wartości. Intuicją za tym czemu zbyt duży `batch size` nie jest w stanie dobrze generalizować tj. uzyskuje niskie wyniki dla metryki na testsetcie, przedstawiono w pracy [Nitish Shirish Keskar](https://arxiv.org/abs/1609.04836). Mówi ona tym, że modele trenowane dużymi `batch size` mają tendencję do wpadania w [`sharp minima`](Visualising the Loss Landscape of Neural Nets), czyli regiony na powierzchni funkcji straty gdzie na bardzo małym obszarze wartość tej funkcji spada do bardzo małych wartości. Jednak ta interpretacją może być nie na miejscu dla sieci o jednej warstwie dwu neuronowej. Natomiast w przypadku bs=1 sieć najprawdopodobniej nie zdążyła jeszcze zbiec.

![png](act_func.png)

	Prawdopodobnie za bardzo dobrymi wynikami funkcji aktywacji tanh, które widźmy na powyższym wykresie, stoi sposób w jaki został wygenerowany data set, czyli zbiór punktów wygenerowany przez funkcję `cube`, która ma kształt podobny do tanh. Relu po pewnym czasie zbiega do dobrego wyniku natomiast. W przypadku sigmoidu sieć nie jest w stanie dopasować się do danych

![png](reg_init.png)

	W przeciwieństwie do wyników dla klasyfikacji losowa inicjalizacja wag uzyskała najlepsze wyniki względem metryk. Na podstawie tego możemy spodziewać się, że zachowanie sieci zainicjalizowanej w ten sposób będzie najbardziej nieprzewidywalne. Z pozostałych funkcji jedynie Xavier zaczna zbiegać

## Wizualizacja stanu sieci

Do wizualizacji struktury sieci wykorzystaliśmy pythonową bibliotekę networkx. Możliwe jest zaprezentowanie wag krawędzi oraz ich zmian w kolejnych iteracjach uczenia. Przykładowa sieć poniżej:

![png](wizualizacja_arch.png)

## Wizualizacja procesu uczenia

Proces uczenia zostanie zwizualizowac na dwa sposoby:
- Historia błędów na danych warstwach
- Predykcje przed i po treningu

### Historia błędów na danych warstwach

Dla każdej warstwy w sieci poza wyjściowa zostanie przedstawiony wykres, który na osi Y przedstawia w jak dużym stopniu warstwa przyczynia się do finalnego błędu w predykcji. Jesteśmy w stanie obliczyć ja z dokładnością do poszczególnych neuronów jednka na potrzeby wizualizacji wartość błędów zostały zsumowane po wszystkich neuronach w warstwie. Na osi X znajduje się numer aktualizacji wag sieci

Parametry sieci

- `activation_func`: 'relu',
- `batch_size`: 10,
- `init`: 'he',
- `loss_func`: ‘MSE’,
- `lr`: 0.1,
- `layer_shapes`: [2, 4  4, 2]

### Wizualizacja historii bledow na warstwach

![png](output_9_0.png)

	W przypadku warstwy pierwszej widać drobny wzrost, a następnie spadek wynikającego z niej błędu

![png](output_9_1.png)

	Dla warstwy drugiej obserwujemy zmniejszenie się wariancji krzywej natomiast wartość błędu pozostaje na tym samym poziomie


![png](output_9_2.png)

	W przypadku ostatniej warstwy widzimy znaczący spadek w wartości błędu oraz wariancji krzywej



### Wnioski

Z historii bledow wynikajacych z kazdej z warstw mozemy wywnioskowac, że przy uczeniu algorytmem propagacji wstecznej najszybciej minimalizowany jest błąd z ostatnich warstw sieci.


## Wizualizacje predykcji modelu

W tym podpunkcie zostaną przedstawione predykcję modelu przed i po treningu na danych treningowych i testowych.

### Regresja

#### Opis eksperymentu
W poniższym eksperymencie z wizualizowane zostały predykcję modelu wytrenowanego na danych z pliku `data.cube.train.500.csv`. Jako dane testowe zostały użyte dane z `data.cube.test.500.csv`. Predykcje modelu i poprawne wyniki zostały zaprezentowane jako punkty na wykresie i podpisane odpowiednio Y i Y_hat

#### Parametry sieci uzytej do regresji
- layer_shapes = [1, 8, 8, 1]
- init = 'he'
- activation_func = 'relu'
- loss_func = 'MSE'
- test_frac = 0.02
- epochs = 200
- batch_size = 10
- lr = 0.01


#### Predykcje przed treningiem



![png](output_15_1.png)



	Zgodnie z oczekiwaniami sieć przed wytrenowaniem zwraca losowe wartości




#### Predykcje po treningu



![png](output_17_1.png)



	Po wytrenowaniu sieci udało się z wysoka dokładnością przewidzieć poprawne wyniki



#### Predykcje na testsetcie

![png](output_19_1.png)

	Jednak przy predykcji na danych nie widzianych wcześniej przed model tj. wartości z osi X, dla których jesteśmy w stanie zaobsorwować „niebieskie ogony” odstające od pomarańczowej, krzywej widzimy, że model nie nauczył się dobrze generalizować

### Klasyfikacja

#### Opis eksperymentu
W przypadku klasyfikacji poprawna klasa została zaznaczoną na wykresie poprzez kształt punktu tj. kółko, kwadrat lub krzyżyk. Predykcję modelu reprezentuję kolor punktu. Dane do treningu i testu pochodzą z plików `data.three_gauss.train.500.csv`, `data.three_gauss.test.500.csv`.

#### Parametry sieci uzytej do klasyfikacji
- layer_shapes = [2, 4, 3]
- init = 'random'
- activation_func = 'sigmoid'
- loss_func = 'MSE'
- metric = metrics.accuracy
- epochs = 20
- batch_size = 10
- lr = 0.1
- test_frac = 0.02

#### Predykcje przed treningiem




![png](output_22_1.png)



	Tak jak w przypadku klasyfikacji model przed treningiem zwraca losowe wartości jako predykcję



#### Predykcje na trainsetcie po treningu


![png](output_24_1.png)



	Widać ze sieci udało się nauczyć rozróżniać trzy chmury gasussowskie z danych treningowych z dokładnością do niepoprawnych przewidywań klasy drugiej na przecięciu trzech chmur punktów



#### Predykcje na testsetcie


![png](output_26_1.png)



	W porównaniu z regresją predykcje modelu klasyfikacyjnego na nie widzianych wcześniej danych są dużo lepsze. Wciąż niepoprawnie klasyfikowane są punkty klasy drugiej na granicy zbiorów. Punkty te sa jednak równie nieliczne co przy predykcji dla `testset`. Jedynym co wzbudza wątpliwość jest to czy dane z `testset` dostatecznie dobrze symulują dane pochodzące spoza próbki. Jeśli nie to próba oceny tego jak dobrze model generalizuję za ich pomocą byłaby skazana na porażkę

# Wnioski końcowe

W celu sprawdzenia wpływu paramterów na proces uczenia sieci neuronowej optymalizowanej metodą Stochastic Gradient Descent zaimplementowaliśmy pipeline, który pozwalał nam na wytrenowanie i zapisanie wyników eksperymentów dla zadanych zbiorów paramterów. Na podstawie przeprowadzonych eksperymentów możemy stwierdzić, że wybrane parametry mają bardzo duży wpływ na przebieg treningu sieci. Sprawdziliśmy wpływ zdefiniowanych wcześniej paramterów `lr`, `batch_sizę`, `init`,`loss_func`, `activation_func`, `layer_shapes` na trzy metryki `test_loss`, `train_loss` , `test_metric_result` i przedstawiliśmy te zależność wraz z wnioskami w postaci wykresów. Następnie zwizualizowaliśmy architekturę i stan sieci używając biblioteki networkx. Ostatnim co zrobiliśmy było przedstawienie procesu uczenia poprzez wyrysowanie historii wkładu w błąd predykcji każdej z warstw w sieci i pokazanie wyników predykcji przed, po treningu i na `testset`. Podstawowym wynikiem wszystkich eksperymentów jest konkluzja, że perceptron wielowarstwowy, pomimo prostej architektury, jest w stanie skutecznie rozwiązywać zagadnienia klasyfikacji i regresji, jednak parametry sieci mają bardzo istotny, często nieintuicyjny i wymagający przeprowadzenia eksperymentów, wpływ na jakość predykcji.