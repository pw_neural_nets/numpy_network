def extract_types(grid, mode, filters, params, rev):
    orignal_copy = deepcopy(filters)

    tested_pars = {}
    for k, list_v in params.items():
        tested_pars[k] = {}
        filters = deepcopy(orignal_copy)
        for v in list_v:
            filters[k] = v
            result = grid.get_top_n_exp_with_given_par(n=1, mode=mode, params=filters, rev=rev)
            if result:
                best = result[0]
                if isinstance(v, list): v = str(v)
                tested_pars[k][v] = best

    return tested_pars


def get_plot_vales(exp_log, p_val, analized_par, plot_name):
    y_ticks = exp_log
    x_ticks = list(range(1, len(exp_log) + 1))

    if analized_par == 'batch_size' and plot_name == 'training_loss':
        x_ticks = [x * p_val for x in x_ticks]

    return x_ticks, y_ticks


def visualize_part_plots(exps, analized_par):
    fig, axs = plt.subplots(3, figsize=(10, 12))
    for ax_nb, plot_name in enumerate(log_data_names2plot):

        for p_val, exp in exps.items():
            exp_logs = exp['experiemnt_logs']
            plot_values = get_plot_vales(exp_logs[plot_name], p_val, analized_par, plot_name)

            axs[ax_nb].plot(*plot_values, label=p_val, alpha=0.8)
        axs[ax_nb].set_title(plot_name)
        axs[ax_nb].legend()

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    fig.suptitle(f'tested parameter: {analized_par}', fontsize=16)

    plt.show()


def plot_weights_change(weight_change, par_type):

    for phase, x in weight_change.items():
        fig, axs = plt.subplots(1,3, figsize=(16,4))
        fig.suptitle(f'{phase.format(par_type)}', fontsize=16)
        layer = x[par_type]

        for i in range(len(layer)):
            axs[i].imshow(layer[i])
            axs[i].set_yticks(np.array(list(range(layer[i].shape[0]))))
            axs[i].set_xticks(np.array(list(range(layer[i].shape[1]))))

            axs[i].set_yticklabels(labels=[f'n_{c}' for c in range(1, layer[i].shape[0]+1)])
            axs[i].set_xticklabels(labels=[f'n_{c}' for c in range(1, layer[i].shape[1]+1)])

            axs[i].set_title(f'layer {i+1} {par_type}')

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])


log_data_names2plot = ['test_loss', 'training_loss', 'test_metric_results']

best_global_parmas = {
    'activation_func': 'relu',
    'batch_size': 10,
    'init': 'he',
    'lr': 0.1,
    'layer_shapes': [2, 8, 8, 3],
}

tested_parameters = {
    'lr': [10, 1, 1e-1, 1e-2],
    'batch_size': [1, 10, 100],
    'activation_func': ['relu', 'sigmoid'],
    'init': ['he','random'],
}


# weight_change = {
#     '{}_before_training': weights_before_training,
#     '{}_during_training': weights_during_training,
#     '{}_after_training' : weights_after_training
# }
