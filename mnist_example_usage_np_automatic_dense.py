import random
import numpy as np
import pandas as pd
from pathlib import Path
import matplotlib.pyplot as plt
import pickle

from data_processing import model_input
from network import np_automatic_dense
from network import metrics
from network import optimizers
from network import schedulers
from pipelines import single_experiment

SEED = 2137
random.seed(SEED)
np.random.seed(SEED)


# paths
ROOT_DIR = Path(".")
CLS_FILE = ROOT_DIR/"mnist/train_mnist.pkl"

with open(CLS_FILE, 'rb') as f:
    X, Y = pickle.load(f)
Y = np.expand_dims(Y, axis=1)
train_data = np.hstack([X, Y])

mode = 'cls'

# net hpars
layer_shapes = [784, 200, 10]
init = 'he'
activation_func = 'relu'
loss_func = 'MSE'
metric = metrics.accuracy
l1 = 0
l2 = 0

# train hpars
start_lr = 0.5
end_lr = 0.2
epochs = 10
batch_size = 10
test_frac = 0.2
strat = 'cos'

sample_count = len(train_data)

lr_scheduler = schedulers.LearningScheduler(
    start_lr=start_lr,
    end_lr=end_lr,
    sample_count=sample_count,
    epochs=epochs,
    test_frac=test_frac,
    batch_size=batch_size,
    strat=strat,
    warmup_frac=0.00,
)


model = np_automatic_dense.DenseNetwork(
    layer_shapes=layer_shapes,
    loss_func=loss_func,
    activation_func=activation_func,
    init=init,
    mode=mode,
    l1=l1,
    l2=l2,
)

preprocessor = model_input.preprocessors[mode](test_frac, verbose=True, scale=False, norm_labels=False)
optimizer = optimizers.OptimizerSGD()
lr_scheduler = iter(lr_scheduler)

experiment_pipe = single_experiment.ExperimentPipeline(
    model=model,
    preprocessor=preprocessor,
    optimizer=optimizer,
    lr_scheduler=lr_scheduler,
    metric=metric,
    verbose=True
)

experiment_pipe.train(train_data, epochs, batch_size)


CLS_FILE_TEST = ROOT_DIR/"mnist/test_mnist.pkl"

with open(CLS_FILE_TEST, 'rb') as f:
    X_test, Y_test = pickle.load(f)

Y_hat = experiment_pipe.predict(X_test)

score = np.sum(Y_hat == Y_test) / len(Y_test)
print('Testset score', score)

train_loss = experiment_pipe.training_loss
test_loss = experiment_pipe.test_loss

trained_model = {
    'model': experiment_pipe.model,
    'preprocessor': experiment_pipe.preprocessor,
}

with open(f"trained_model_strat_cos_size_{200}_acc_{score}.pkl", 'wb') as f:
    pickle.dump(trained_model, f)
