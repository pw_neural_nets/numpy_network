import random
import numpy as np
from pathlib import Path
import pickle

from pipelines import single_experiment

SEED = 2137
random.seed(SEED)
np.random.seed(SEED)

def load_model(model_path):
    with open(model_path, 'rb') as f:
        trained_model = pickle.load(f)

    experiment_pipe = single_experiment.ExperimentPipeline(
        model=trained_model['model'],
        preprocessor=trained_model['preprocessor'],
        optimizer=None,
        lr_scheduler=None,
        metric=None,
        verbose=True
    )

    return experiment_pipe

ROOT_DIR = Path(".")
MODEL_PATH = ROOT_DIR/"trained_model_size_30_acc_0.9541.pkl"  # "trained_model_strat_cos_size_200_acc_0.9787.pkl"
CLS_FILE_TEST = ROOT_DIR/"mnist/test_mnist.pkl"

with open(CLS_FILE_TEST, 'rb') as f:
    X_test, Y_test = pickle.load(f)

experiment_pipe = load_model(MODEL_PATH)

Y_hat = experiment_pipe.predict(X_test)

score = np.sum(Y_hat == Y_test) / len(Y_test)
print('Testset score', score)

train_loss = experiment_pipe.training_loss
test_loss = experiment_pipe.test_loss
