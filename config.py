from pathlib import Path

hparams_grid = {
    'lr': [1e-1],
    'batch_size': [10],
    'epochs': [5],
    'loss_func': ['MSE'],
    'activation_func': ['relu'],# 'relu, tanh, sigmoid
    'init': ['he']
}

lrs = 2
neurons = 2

net_arch = {
    'layer_size_multipliers': [3],
    'layer_count': [2],
}

ROOT_DIR = Path("data")
CLS_DIR = ROOT_DIR/"classification"
CLS_FILE = [CLS_DIR/"data.simple.train.500.csv"]
CLS_FILES = list(CLS_DIR.glob('**/*train.500*.csv'))

REG_DIR = ROOT_DIR/"regression"
REG_FILE = [REG_DIR/"data.activation.train.500.csv"]
REG_FILES = list(REG_DIR.glob('**/*train.500*.csv'))

task_files = {'cls': CLS_FILES, 'reg': REG_FILES}

verbose = False
test_frac = 0.2

results_path = Path(f'./expeiment_results_MSE_MAE_arch_c1,2_s2,10')
