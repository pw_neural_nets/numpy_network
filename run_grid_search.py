import config

from data_types import results_grid
from pipelines import grid_search
import pickle

# set experiemnt parameters in config

print(config.hparams_grid)
print(config.net_arch)

gsearch = grid_search.GridSearch.from_config(config)
gsearch.pipe_files()

# print top result parameter in this dataset

# for file in config.CLS_FILES:
#     with open(f'./expeiment_results/cls/experiment_{file.name}.pkl', 'rb') as f:
#         grid = pickle.load(f)
#
#     top_exp_s = grid.get_top_n_exp_with_given_par(n=1, mode='cls', params={})
#     best = top_exp_s[0]
#     print(best['experiemnt_logs']['test_metric_results'])
#     print(best['hparams'])
