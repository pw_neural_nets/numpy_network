import numpy as np
import pandas as pd
from sklearn.preprocessing import StandardScaler


class InputPreprocessor:
    '''Convert input data into Model specific input format'''

    def __init__(self, test_frac, verbose, scale=True):
        self.test_frac = test_frac
        self.scaler_X = StandardScaler()
        self.fitted = False
        self.verbose = verbose
        self.scale = scale

    def _process(self, data):
        raise NotImplemented()

    def prepare_X(self, X):
        return self.scale_X(X)

    def prepare_Y(self, Y):
        raise NotImplemented()

    def scale_X(self, X):
        return self.scaler_X.transform(X)

    def scale_Y(self, Y):
        return self.scaler_Y.transform(Y)

    def inv_scale_X(self, X):
        return self.scaler_X.inverse_transform(X)

    def inv_scale_Y(self, Y):
        raise NotImplemented()

    def preprocess_training_data(self, df):
        data = df

        X_train, Y_train, X_test, Y_test = self._process(data)

        X_train = np.expand_dims(X_train, -1)
        X_test = np.expand_dims(X_test, -1)

        self.fitted = True

        return X_train, Y_train, X_test, Y_test

    def train_test_split(self, array):
        split_idx = int(len(array) * self.test_frac)
        test = array[:split_idx]
        train = array[split_idx:]
        return train, test

    def X_Y_split(self, array):
        return array[:, :-1], array[:, -1]


class RegPreprocessor(InputPreprocessor):
    def __init__(self, test_frac, verbose, scale=True):
        super().__init__(test_frac, verbose, scale)
        self.scaler_Y = StandardScaler()

    def prepare_X(self, X):
        X = np.array(X)
        X = np.expand_dims(X, -1)
        if self.scale: X = self.scaler_X.transform(X)
        return X


    def inv_scale_Y(self, Y):
        Y = np.array(Y)
        Y = Y.reshape(-1, 1)
        if self.scale: Y = self.scaler_Y.inverse_transform(Y)
        return Y

    def prepare_Y(self, Y):
        Y = Y.reshape(-1, 1)
        if self.scale: Y = self.scale_Y(Y)
        Y = Y.reshape(-1, 1, 1)
        assert len(Y.shape) == 3
        return Y

    def fit_Y(self, Y):
        Y = Y.reshape(-1, 1)
        self.scaler_Y.fit(Y)

    def _process(self, data):
        train, test = self.train_test_split(data)

        X_train, Y_train = self.X_Y_split(train)
        X_test, Y_test = self.X_Y_split(test)

        self.scaler_X.fit(X_train)
        self.fit_Y(Y_train)

        if self.scale:
            X_train = self.scale_X(X_train)
            X_test = self.scale_X(X_test)

        Y_test = self.prepare_Y(Y_test)
        Y_train = self.prepare_Y(Y_train)

        return X_train, Y_train, X_test, Y_test


class ClsPreprocessor(InputPreprocessor):
    def __init__(self, test_frac, verbose, norm_labels=True, scale=True):
        super().__init__(test_frac, verbose, scale)
        self.norm_labels = norm_labels

    def prepare_X(self, X):
        if self.scale: X = self.scaler_X.transform(X)
        X = np.expand_dims(X, -1)
        return X

    def prepare_Y(self, Y):
        Y = self.normalize_cls_lables(Y)
        Y = self.one_hot_encode_classes(Y)
        return Y

    def inv_scale_Y(self, Y):
        return Y

    def _process(self, data):
        X, Y = self.X_Y_split(data)

        Y = self.prepare_Y(Y)

        Y_train, Y_test = self.train_test_split(Y)
        X_train, X_test = self.train_test_split(X)

        if self.scale: self.scaler_X.fit(X_train)

        if self.scale:
            X_train = self.scale_X(X_train)
            X_test = self.scale_X(X_test)

        return X_train, Y_train, X_test, Y_test

    def one_hot_encode_classes(self, Y):
        Y_dummy = pd.get_dummies(Y).values
        return np.expand_dims(Y_dummy, -1)

    def normalize_cls_lables(self, Y):
        if self.norm_labels:
            if self.verbose:
                print('subtracting one from cls column to normalize labels')
            return Y - 1
        else:
            return Y


preprocessors = {
    'cls': ClsPreprocessor,
    'reg': RegPreprocessor,
}