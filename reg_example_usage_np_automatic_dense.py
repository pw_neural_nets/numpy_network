import random
import numpy as np

SEED = 2137

random.seed(SEED)
np.random.seed(SEED)

from pathlib import Path
import pandas as pd
import matplotlib.pyplot as plt

from data_processing import model_input
from network import np_automatic_dense
from network import metrics
from network import optimizers
from pipelines import single_experiment

# regression

ROOT_DIR = Path("projekt1-oddanie")
REG_DIR = ROOT_DIR/"regression"
REG_FILE = REG_DIR/"data.multimodal.train.500.csv"

# net hpars
layer_shapes = [1, 20, 20, 20, 20, 20, 20, 1]
init = 'he'
activation_func = 'relu'
loss_func = 'MSE'
mode = 'reg'
test_frac = 0.01

# train hpars
epochs = 100
batch_size = 1
lr = 0.001
metric = metrics.distance

df_reg = pd.read_csv(REG_FILE)
df_reg = df_reg.sort_values(by=['x'])

model = np_automatic_dense.DenseNetwork(
    layer_shapes=layer_shapes,
    loss_func=loss_func,
    activation_func=activation_func,
    init=init,
    mode=mode
)

preprocessor = model_input.preprocessors[mode](test_frac, verbose=True)
optimizer = optimizers.OptimizerSGD()
experiment_pipe = single_experiment.ExperimentPipeline(model, preprocessor, optimizer, metric, verbose=True)

experiment_pipe.train(df_reg.values, epochs, batch_size, lr)

X = df_reg['x'].values
Y_hat = experiment_pipe.predict(X)

plt.plot(df_reg['x'], Y_hat, label='Y_hat')
plt.plot(df_reg['x'], df_reg['y'], label='Y')
plt.legend()
plt.show()

#
# # test prediction
# REG_TEST_FILE = REG_DIR/"data.multimodal.test.500.csv"
# test_df_reg = pd.read_csv(REG_TEST_FILE)
#
# X = test_df_reg['x']
# Y_hat = experiment_pipe.predict(X)
#
# plt.scatter(test_df_reg['x'], Y_hat, label='Y_hat', alpha=0.5)
# plt.scatter(test_df_reg['x'], test_df_reg['y'], label='Y', alpha=0.5)
# plt.legend()
# plt.show()
