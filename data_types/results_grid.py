def sort_cls(exp):
    metric_log = exp['experiemnt_logs']['test_metric_results']
    loss_log = exp['experiemnt_logs']['test_loss']
    return max(metric_log), min(loss_log)


def sort_reg(exp):
    metric_log = exp['experiemnt_logs']['test_metric_results']
    return min(metric_log)


sort_func = {
    'cls': sort_cls,
    'reg': sort_reg,
}


class ResultsGrid:
    '''params = {'lr': 1, 'init': 'he'}

    Strongly coupled with single_experiment via this
         'training_loss': experiment_pipe.training_loss,
         'test_loss': experiment_pipe.test_loss,
         'layers_errors': experiment_pipe.layers_errors,
         'test_metric_results': experiment_pipe.test_metric_results,
    '''

    def __init__(self):
        self.experments_results = []

    def add_exp(self, mode, experiemnt_logs, hparams):
        exp_res = {
            'mode': mode,
            'experiemnt_logs': experiemnt_logs,
            'hparams': hparams,
        }

        self.experments_results.append(exp_res)

    def get_experiments_with_hparams(self, mode, params):
        exps = []
        for exp in self.experments_results:
            if (exp['mode'] == mode
                    and all([exp['hparams'][k] == v
                             for k, v in params.items()])):
                exps.append(exp)
        return exps

    def get_top_n_exp_with_given_par(self, n, mode, params, rev=False):
        exps = self.get_experiments_with_hparams(mode, params)
        exps = sorted(exps, key=sort_func[mode], reverse=rev)
        return exps[-n:]

