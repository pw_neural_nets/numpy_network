import random
import numpy as np

class ExperimentPipeline:
    '''
    Perform process of model traning given parameters. Process of traning consists of:
    1. show model the data traning_data to fit the preprocessor
    2. split data and feed it into the model
    3. use optimizer to optimize the model
    4. save the state of a model after each step (loss, layer_errors, weights)
    5. evaluate the model predictions with metrics after each epoch
    6. return/save best model picked by metric? (mb later)
    '''

    def __init__(self, model, preprocessor, optimizer, metric, lr_scheduler, verbose=True, track_specific=False):
        self.model = model
        self.preprocessor = preprocessor
        self.optimizer = optimizer
        self.lr_scheduler = lr_scheduler

        self.metric = metric
        self.verbose = verbose
        self.track_specific = track_specific

        self.training_loss = []
        self.test_loss = []
        self.layers_errors = []
        self.test_metric_results = []
        self.model_parameters = []

    def train(self, data, epochs, batch_size):

        X_train, Y_train, X_test, Y_test = self.preprocessor.preprocess_training_data(data)
        training_data = list(zip(X_train, Y_train))

        n = len(X_train)
        for j in range(epochs):
            random.shuffle(training_data)

            batches = [
                training_data[k:k + batch_size]
                for k in range(0, n, batch_size)]

            for batch in batches:
                lr = next(self.lr_scheduler)
                layers_error, batch_loss = self.optimizer.optimize(self.model, batch, lr)
                if self.track_specific:
                    self.layers_errors.append(layers_error)
                    self.model_parameters.append({'weights': self.model.weights, 'biases': self.model.biases})
                self.training_loss.append(batch_loss)

            if X_test is not None:
                self.evaluate(X_test, Y_test)

            if self.verbose:
                print(f"Epoch {j + 1}/{epochs} complete")

    def predict(self, X):
        X = self.preprocessor.prepare_X(X)
        Y_hat = self.model.predict(X)
        Y_hat = self.preprocessor.inv_scale_Y(Y_hat)
        return Y_hat

    def evaluate(self, x, y):
        y_hat = np.array([self.model.predict_logit(v) for v in x])
        y = np.array(y)

        loss = self.model.loss_func.get_loss(y_hat, y)
        score = self.metric(y_hat, y)

        self.test_loss.append(loss)
        self.test_metric_results.append(score)

        if self.verbose:
            self.log_results()

    def log_results(self):
        print(f'loss: {self.test_loss[-1]}')
        print(f'metric: {self.test_metric_results[-1]}')
