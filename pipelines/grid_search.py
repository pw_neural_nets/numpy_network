import pandas as pd
from copy import deepcopy

import itertools
from data_processing import model_input
from network import optimizers
from network import np_automatic_dense
from network import metrics
from data_types import results_grid as rg

from pipelines import single_experiment

import pickle
from tqdm import tqdm


## TODO

# add evaluation score on test_sets from folder with data

# add saving experiments after N experiments and clearing memory

# paralelize

# bucket results into top n
# (not simply order by best metric score cuz
# the scores might be very similar and we want to see what
# arch got this type of similar scores)

# add option constant number of neurons which would filter those
# architectures which sum is bigger than given threshold
# so that architechures are comparable

# add running experiemnts multipe times to elimenate randomness
# and get avg +- std, eliminate accidental divergenves
# (ignore nan in mean but keep track of how many there were in exp)

# PCA to select most imprtant parameters/ clustering parameters by metric scores
# (so train a Par_model to predict metric score/loss of a Exp_model and see the weights
# makes not much sense cuz it depends on data. Gen is prob better)

class GridSearch:
    '''
    1. Run experiments on given dataset for every combination of hparams.
    They specify model arch and traning process
    2. Visalize results

    2. best combination for each of provided paramters? that would be a lot.
    Details of top 3 best?
    Create a grid and then you can check what you want e.g. best batch size, best init or just best result
    '''

    def __init__(self, files_per_type, net_arch, expriment_params_list, results_path, test_frac, verbose=False):
        self.files_per_type = files_per_type
        self.expriment_params_list = expriment_params_list
        self.net_arch = net_arch
        self.metrics = {'cls': metrics.accuracy, 'reg': metrics.distance}
        self.verbose = verbose
        self.results_path = results_path
        self.test_frac = test_frac

    @classmethod
    def from_config(cls, config):

        all_params = sorted(config.hparams_grid)
        param_combinations = itertools.product(
            *(config.hparams_grid[param] for param in all_params))

        exp_params = [{k: v for k, v in zip(all_params, par_comb)}
                      for par_comb in param_combinations]

        net_arch = config.net_arch
        results_path = config.results_path
        test_frac = config.test_frac

        return cls(
            files_per_type=config.task_files,
            expriment_params_list=exp_params,
            net_arch=net_arch,
            verbose=config.verbose,
            test_frac=test_frac,
            results_path=results_path,
        )

    def save_experiment_result(self, experments_results, file_name, task_type):
        folder = self.results_path / task_type

        if not folder.exists():
            folder.mkdir(parents=True)

        with open(folder / f'experiment_{file_name}.pkl', 'wb') as f:
            pickle.dump(experments_results, f)

    def pipe_files(self):

        for task_type in tqdm(self.files_per_type):
            for file in tqdm(self.files_per_type[task_type]):
                experments_results = self.run_paramterized_experiments(file, mode=task_type)
                self.save_experiment_result(experments_results, file.name, task_type)

    def run_paramterized_experiments(self, file, mode):
        df = pd.read_csv(file)

        net_archs = self.get_layer_arch_grid(df, mode)

        results_grid = rg.ResultsGrid()
        for layer_shapes in net_archs:
            for hparams in self.expriment_params_list:
                hparams = deepcopy(hparams)
                hparams['layer_shapes'] = layer_shapes

                self.log(file, mode, hparams)
                experiemnt_logs = self.run_nn_experiment(
                    df=df,
                    mode=mode,
                    test_frac=self.test_frac,
                    **hparams,
                )

                results_grid.add_exp(
                    mode,
                    experiemnt_logs,
                    hparams,
                )

        return results_grid

    def log(self, file, mode, hparams):
        if self.verbose:
            print('\n\n -------------')
            print(f'file: {file}')
            print('mode: ', mode)
            for k, v in hparams.items():
                print(f'{k}: {v}')

            print('-' * 8)

    def get_output_size(self, df, mode):
        if mode == 'cls':
            n_class = len(df['cls'].unique())
            return n_class
        else:
            return 1

    def get_layer_arch_grid(self, df, mode):

        output_size = self.get_output_size(df, mode)

        input_shape = df.shape[1] - 1
        layer_shapes = [x * input_shape for x in self.net_arch['layer_size_multipliers']]

        experiemnts_net_layers = []
        for layer_count in self.net_arch['layer_count']:
            for hidden_layers in set(itertools.product(layer_shapes, repeat=layer_count)):
                net_layers = [input_shape, *hidden_layers, output_size]
                experiemnts_net_layers.append(net_layers)

        return experiemnts_net_layers

    def run_nn_experiment(
            self, df, mode, layer_shapes, loss_func,
            activation_func, init,
            epochs, batch_size, lr,
            test_frac,
    ):

        preprocessor = model_input.preprocessors[mode](test_frac, self.verbose)
        optimizer = optimizers.OptimizerSGD()
        metric = self.metrics[mode]

        model = np_automatic_dense.DenseNetwork(
            layer_shapes=layer_shapes,
            loss_func=loss_func,
            activation_func=activation_func,
            init=init,
            mode=mode,
        )

        experiment_pipe = single_experiment.ExperimentPipeline(
            model,
            preprocessor,
            optimizer,
            metric,
            verbose=self.verbose,
        )

        experiment_pipe.train(df, epochs, batch_size, lr)

        experiemnt_results = {
            'training_loss': experiment_pipe.training_loss,
            'test_loss': experiment_pipe.test_loss,
            'layers_errors': experiment_pipe.layers_errors,
            'test_metric_results': experiment_pipe.test_metric_results,
            'model_parameters': experiment_pipe.model_parameters,
        }

        return experiemnt_results
